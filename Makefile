all: normal blas

normal: eliminacao_de_gauss.c
	gcc eliminacao_de_gauss.c -o normal

blas: eliminacao_de_gauss_blas.c
	gcc eliminacao_de_gauss_blas.c -o blas -lcblas
