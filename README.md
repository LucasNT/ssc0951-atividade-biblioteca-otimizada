the c program [eliminacao_de_gauss.c](eliminacao_de_gauss.c) don't need any external library to compile. the [eliminacao_de_gauss_blas.c](eliminacao_de_gauss_blas.c) needs a blas library to compile and run.

The input of the programs has two numbers in the first line, one for the amount of the linear systems and and the other to the amount of variables, in the sequence is the coeficients of the linear systems and the results, example:

the Linear system:

$`a_{11}*x_1 + a_{12}*x_2 + a_{13}*x_3 = b_1`$

$`a_{21}*x_1 + a_{22}*x_2 + a_{23}*x_3 = b_2`$

$`a_{31}*x_1 + a_{32}*x_2 + a_{33}*x_3 = b_3`$

will be:

3 3

$`a_{11}`$  $`a_{12}`$ $`a_{13}`$ $`b_1`$

$`a_{21}`$  $`a_{22}`$  $`a_{23}`$  $`b_2`$

$`a_{31}`$  $`a_{32}`$  $`a_{33}`$  $`b_3`$
