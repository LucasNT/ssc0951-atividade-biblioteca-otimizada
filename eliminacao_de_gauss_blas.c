#include<stdio.h>
#include <stdlib.h>
#include <cblas.h>


void free_matrix(int m_row , double **matrix);
double *eliminacao_de_gauss(int m_row , int n_col , double** matriz_A , double* vec_B);
void leitura_matriz(int m_row , int n_col , double **matriz , double *vec , FILE *input);
double **create_matrix(FILE *input , int *m_row , int *n_col);
void print_matrix(FILE *output , int m_row , int n_col , double **matrix);
void print_vec(FILE *output , int m_row , double *vec);




void print_vec(FILE *output , int m_row , double *vec){
	for(int i = 0 ; i < m_row ; i++){
		printf("%lf " , vec[i]);
	}
	printf("\n");
}

void print_matrix(FILE *output , int m_row , int n_col , double **matrix){
	for(int i = 0 ; i < m_row ; i++){
		double *aux = matrix[i];
		for(int j = 0; j < n_col ; j++){
			printf("%lf " , aux[j]);
		}
		printf("\n");
	}
}

double *eliminacao_de_gauss(int m_row , int n_col , double** matriz_A , double* vec_B){
	double *resu = aligned_alloc(8 , sizeof(double) * n_col);

	int stop_first_loop = m_row-1;
	for(int i = 0; i < stop_first_loop ; i++){
		double *row_i = matriz_A[i];
		double val_diagonal = row_i[i];
		if(val_diagonal == 0 ){
			printf("Erro\n");
			exit(1);
		}
		for(int j = i+1;  j < m_row ; j++){
			double *row_j = matriz_A[j];
			double alpha = - row_j[i] / val_diagonal;
			cblas_daxpy(n_col - i , alpha , row_i + i , 1 , row_j + i , 1);
			vec_B[j] = vec_B[i] * alpha + vec_B[j];
		}
	}
	resu[m_row-1] = vec_B[m_row -1] / matriz_A[m_row-1][n_col-1];
	for(int i = m_row -2 ; i >= 0 ; i--){
		double *row_i = matriz_A[i];
		resu[i] = (vec_B[i] - cblas_ddot(m_row - i -1 , row_i + i + 1 , 1 , resu + i + 1,1))/row_i[i];
	}


	return resu;
}

void leitura_matriz(int m_row , int n_col , double **matriz , double *vec , FILE *input){
	for(int i = 0 ; i < m_row ; i++){
		double *aux = matriz[i];
		for(int j = 0 ; j < n_col ;j++){
			fscanf(input , "%lf" , &aux[j]);
		}
		fscanf(input , "%lf" , vec + i);
	}
}

double **create_matrix(FILE *input , int *m_row , int *n_col){
	double **matrix;
	fscanf(input , "%d %d" , m_row , n_col);
	matrix = aligned_alloc( 8 , sizeof(double) * (*m_row));
	for(int i = 0 ; i < *m_row ; i++){
		matrix[i] = aligned_alloc( 8 , sizeof(double) * (*n_col));
	}
	return matrix;
}

void free_matrix(int m_row , double **matrix){
	for(int i = 0; i < m_row ; i++){
		free(matrix[i]);
	}
	free(matrix);
}

int main (int argc , char* argv[]){
	double **matriz;
	double *vec;
	double *resu;
	int m_row , n_col;
	matriz = create_matrix(stdin , &m_row , &n_col);
	vec = malloc(sizeof(double) * m_row);
	leitura_matriz(m_row , n_col , matriz , vec  , stdin);
	resu = eliminacao_de_gauss(m_row ,n_col , matriz , vec);
	print_vec(stdout , m_row , resu);
	free(vec);
	free_matrix(m_row , matriz);
	free(resu);
	return 0;
}
